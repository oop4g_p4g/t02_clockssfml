#pragma once

#include <string>

struct Dim2Di
{
	int x, y;
};

struct Dim2Df
{
	float x, y;
};

 

namespace GC
{
	//game play related constants to tweak
	static const Dim2Di SCREEN_RES{ 1200,800 };

	static const char ESCAPE_KEY{ 27 };
}


