/*
**A simple library of random numbers routines  
*/

#pragma once

/*
Seed() - seed the random number generator from current system time 
or a specific value that can force the same sequence, good for debugging 
or procedurally generating somethign predictable
seed - ignore to use system time
*/
void Seed(int seed=-1);

//Random(max) - produce a random number in range [1..max]
//pre-condition: max > 0 && max < RAND_MAX
int Random(int max); 

/*
Get a random number between the two limits, the random
number could actually be min or max or something inbetween
Two versions for integer or floating point ranges
//pre-con: min<max
*/
float GetRandomRange(float min, float max);
int GetRandomRange(int min, int max);

