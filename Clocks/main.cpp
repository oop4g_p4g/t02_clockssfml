#include <assert.h>
#include <ctime>	//for time
#include <iostream>	//for cin >> and cout <<
#include <sstream>	//for string stream processing
#include <string>	//for string routines
#include <iomanip>	//for setw, setfill

#include "SFML/Graphics.hpp"

#include "GameConsts.h"
#include "RandomUtils.h"

using namespace std;


//--------Time class
class Time {
public:
	Time();			//default constructor
	Time(const Time&);	//copy constructor
	Time(int h, int m, int s);	//bespoke constructor
	Time(long long);		//conversion constructor
	Time(const string& timeAsString);	//conversion constructor
	~Time();			//destructor

	int getHours() const;	//return a data member value, hrs_
	int getMinutes() const;	//return a data member value, mins_
	int getSeconds() const;	//return a data member value, secs_
	void setTime(int, int, int);	//set the time to 3 given values
	void readInTime();	//input time from user
	string displayTime() const;	//display in 00:00:00 digital clock format
	bool isPM() const {
		return (getHours() >= 12);
	}
	void adjustTime(int, int, int); //add some time onto the existing time

private:
	int hrs_, mins_, secs_;
	long long toSeconds() const;	//return the time in seconds
};

//public functions_______________________________________________

void Time::adjustTime(int offHr, int offMin, int offSec)
{
	assert(offHr >= 0 && offHr < 24);
	assert(offMin >= 0 && offMin < 60);
	assert(offSec >= 0 && offSec < 60);

	hrs_ += offHr;
	if (hrs_ >= 24)
		hrs_ -= 24;
	mins_ += offMin;
	if (mins_ >= 60)
	{
		mins_ -= 60;
		hrs_++;
		if (hrs_ >= 24)
			hrs_ -= 24;
	}
	secs_ += offSec;
	if (secs_ >= 60)
	{
		secs_ -= 60;
		mins_++;
		if (mins_ >= 60)
		{
			mins_ -= 60;
			hrs_++;
			if (hrs_ >= 24)
				hrs_ -= 24;
		}
	}
}

int getCurrentHours() {
	time_t now = time(0);
	struct tm t;
	localtime_s(&t, &now);
	return t.tm_hour;
}
int getCurrentMinutes() {
	time_t now = time(0);
	struct tm t;
	localtime_s(&t, &now);
	return t.tm_min;
}
int getCurrentSeconds() {
	time_t now = time(0);
	struct tm t;
	localtime_s(&t, &now);
	return t.tm_sec;
}
Time::Time()	//default constructor
	: hrs_(getCurrentHours()),
	mins_(getCurrentMinutes()),
	secs_(getCurrentSeconds())
{}


//using memberwise initialisation
Time::Time(const Time& t)
	: hrs_(t.hrs_), mins_(t.mins_), secs_(t.secs_)
{
	//cout << "\nCalling Time( const Time& t) copy constructor! ";
}
//Q1
//create Time from 3 integers representing its hours, minutes and seconds
Time::Time(int h, int m, int s)
	: hrs_(h), mins_(m), secs_(s)
{
	//cout << "\nCalling Time(int h, int m, int s) bespoke constructor! ";
}
//create Time from a long long number of seconds
Time::Time(long long s)
	: hrs_(((s / 60) / 60) % 24),
	mins_((s / 60) % 60),
	secs_(s % 60)
{
	//cout << "\nCalling Time(long long) conversion constructor! ";
}
//create Time from a formatted string ("HH:MM:SS")
Time::Time(const string& s) { 	//requires sstream library
	istringstream is_time(s);
	char ch;				//(any) colon field delimiter
	is_time >> hrs_ >> ch >> mins_ >> ch >> secs_;
	//cout << "\nCalling Time(const string& s) conversion constructor! ";
}
//

Time::~Time() {
}
int Time::getHours() const { 	//return a data member value, hrs_
	return hrs_;
}
int Time::getMinutes() const {	//return a data member value, mins_
	return mins_;
}
int Time::getSeconds() const {	//return a data member value, secs_
	return secs_;
}
void Time::setTime(int h, int m, int s) {	//set the time
	hrs_ = h;
	mins_ = m;
	secs_ = s;
}
void Time::readInTime() {  	//read in time form user
	cout << "Enter the hours: ";
	cin >> hrs_;
	cout << "Enter the minutes: ";
	cin >> mins_;
	cout << "Enter the seconds: ";
	cin >> secs_;
}
string Time::displayTime() const {	 //display time (00:00:00)

	stringstream ss;
	ss << std::setfill('0') << setw(2) << hrs_ << ":"
		<< setw(2) << mins_ << ":"
		<< setw(2) << secs_;
	return ss.str();
}

//private support functions_______________________________________________
long long Time::toSeconds() const {	 //return time in seconds
	return ((hrs_ * 3600) + (mins_ * 60) + secs_);
}

//--------end of Time class


/*
Convert time to hours and minute hand rotation values
hrs24, mins - the time in hours and minutes
degHrs - angle needed of hours hand in degrees
degMins - angle needed of minutes hand in degrees
*/
void TimeToAngle(int hrs24, int mins, float& degHrs, float& degMins)
{
	assert(hrs24 >= 0 && hrs24 < 24);
	assert(mins >= 0 && mins < 60);
	degHrs = (hrs24 > 12) ? (float)hrs24 - 12.f : (float)hrs24;
	degHrs = (360.f / 12.f) * degHrs + (360.f / 12.f) * mins / 60.f;
	degMins = (360.f / 60.f) * mins;
}

/*
Shows a digital clock, the time it shows is random, but the clock does run properly in real time
It then shows three analog clocks, they show three different times, but also run in real time 
so you can see the minute hand moving. The digital clock matches ONE analog clock. The player
has to pick the matching analog clock and can keep going right or wrong indefintely.
*/
class Game
{
public:
	//graphics resources
	struct Gfx
	{
		sf::RenderWindow window;		//for all rendering
		sf::Font fontDigital, fontMain; //fonts for text
		sf::Texture texAnalogFace;		//the old style clock face
		sf::Texture texDigitalFace;		//the new style clock
		sf::Texture texAnalogHands;		//old style clock hands - two sprites on one texture (an atlus)
		sf::Sprite sprHours, sprMinutes;//sprites for the clock hands
	};
	Gfx gfx;

	//time information
	struct TimeSetup
	{
		int hrsMins[6];		//three different random times (hours + mins)
		int correctClock;	//which pair is correct (shown on digital clock and one analog clock)
	};
	TimeSetup timeSetup;

	static const float hoursScale;		//we need a way to scale the analog hours hand sprite
	static const float minutesScale;	//same for the minute hand, they might not fit otherwise
	
	//control what state the game is in
	enum class Mode { 
		WAIT,		//wait for use to select a clock
		WON, 
		LOST
	};
	Mode mode = Mode::WAIT;

	//one time setup, load everything
	void Initiallise(const string& title);
	//update the logic
	void Update();
	//draw everything
	void Render();

private:
	//draw a digital clock
	void DrawDigital(float x, float y, float sx, float sy, const string& timeStr, bool isPM);
	//draw an old style clock
	void DrawAnalog(float x, float y, float sx, float sy, int hours24, int minutes, bool isPM);
	//reset things when a new game is about to start
	void Reset();
	//tell the player what to do
	void ShowInstructions();
	/*
	draw the correct digital time, draw another three analog clocks
	one of these will be correct and two will be wrong
	*/
	void DrawClocks();
};
const float Game::hoursScale = 0.5f;
const float Game::minutesScale = 0.5f;

void Game::Initiallise(const string& title)
{
	Seed();
	gfx.window.create(sf::VideoMode(GC::SCREEN_RES.x, GC::SCREEN_RES.y), title);

	if (!gfx.texDigitalFace.loadFromFile("data/digital_clock.png"))
		assert(false);
	if (!gfx.texAnalogFace.loadFromFile("data/clock_face.png"))
		assert(false);
	if (!gfx.texAnalogHands.loadFromFile("data/clock_hands.png"))
		assert(false);

	//hours hand is one image within the texture atlus
	gfx.sprHours.setTexture(gfx.texAnalogHands);
	sf::IntRect dim{ 10,137,76,348 };
	gfx.sprHours.setTextureRect(dim);
	gfx.sprHours.setOrigin(38, 319);

	//minute hand is another image with the texture atlus
	gfx.sprMinutes.setTexture(gfx.texAnalogHands);
	dim = sf::IntRect{ 95,4,55,475 };
	gfx.sprMinutes.setTextureRect(dim);
	gfx.sprMinutes.setOrigin(27, 454);

	if (!gfx.fontDigital.loadFromFile("data/fonts/digital-7.ttf"))
		assert(false);
	if (!gfx.fontMain.loadFromFile("data/fonts/DroidSans.ttf"))
		assert(false);
	
	//randomise clocks
	Reset();
}

void Game::Reset()
{
	//one of three times is correct
	timeSetup.correctClock = GetRandomRange(0, 2);

	//pick three sets of time offsets
	for (int i = 0; i < 6; i += 2)
		timeSetup.hrsMins[i] = GetRandomRange(0, 23);
	for (int i = 1; i < 6; i += 2)
		timeSetup.hrsMins[i] = GetRandomRange(0, 59);
}


void Game::Update()
{
	switch (mode)
	{
	case Game::Mode::WAIT:
		//did they get it right?
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num1) ||
			sf::Keyboard::isKeyPressed(sf::Keyboard::Num2) ||
			sf::Keyboard::isKeyPressed(sf::Keyboard::Num3))
		{
			if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Num1) && timeSetup.correctClock == 0) ||
				(sf::Keyboard::isKeyPressed(sf::Keyboard::Num2) && timeSetup.correctClock == 1) ||
				(sf::Keyboard::isKeyPressed(sf::Keyboard::Num3) && timeSetup.correctClock == 2))
				mode = Game::Mode::WON;
			else
				mode = Game::Mode::LOST;
		}
		break;
	case Game::Mode::LOST:
	case Game::Mode::WON:
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Y))
		{
			//another go
			Reset();
			mode = Game::Mode::WAIT;
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::N))
		{
			//goodbye
			gfx.window.close();
		}
		break;
	}
}

void Game::ShowInstructions()
{
	sf::Text txt;
	txt.setFont(gfx.fontDigital);
	txt.setCharacterSize(30);
	//instructional messages
	switch (mode)
	{
	case Game::Mode::WAIT:
		txt.setFont(gfx.fontMain);
		txt.setString("Which clock shows the same time as the digital one? 1 or 2 or 3");
		txt.setPosition(10, 600);
		gfx.window.draw(txt);
		break;
	case Game::Mode::LOST:
		txt.setString("Wrong, try again? <Y or N>");
		txt.setPosition(10, 600);
		gfx.window.draw(txt);
		break;
	case Game::Mode::WON:
		txt.setString("Correct, try again? <Y or N>");
		txt.setPosition(10, 600);
		gfx.window.draw(txt);
		break;
	}
}

void Game::DrawClocks()
{
	//draw the correct digital clock
	Time t;
	t.adjustTime(timeSetup.hrsMins[timeSetup.correctClock * 2], timeSetup.hrsMins[timeSetup.correctClock * 2 + 1], 0);
	DrawDigital(400, 100, 0.5f, 0.5f, t.displayTime(), t.isPM());

	//draw three analog clocks
	int cIdx = 0;
	stringstream ss;
	float x = 150, y = 300;
	for (int i = 0; i < 3; ++i)
	{
		//label them
		ss.str("");
		ss << i + 1;

		//draw analog clock face
		sf::Text txt2(ss.str(), gfx.fontMain, 30);
		txt2.setPosition(x - 10.f, y);
		gfx.window.draw(txt2);
		//only one is correct
		Time t2;
		t2.adjustTime(timeSetup.hrsMins[cIdx * 2], timeSetup.hrsMins[cIdx * 2 + 1], 0);
		cIdx++;
		DrawAnalog(x, y, 0.5f, 0.5f, t2.getHours(), t2.getMinutes(), t2.isPM());
		x += 300;
	}
}

void Game::Render()
{
	//title 
	sf::Text txt("Time to play?", gfx.fontDigital, 40);
	txt.setPosition(20, 20);
	gfx.window.draw(txt);

	DrawClocks();

	ShowInstructions();
}

//draw a digital clock
void Game::DrawDigital(float x, float y, float sx, float sy, const string& timeStr, bool isPM)
{
	//background
	sf::Sprite spr(gfx.texDigitalFace);
	spr.setOrigin(0, 0);
	spr.setPosition(x, y);
	spr.setScale(sx, sx);
	gfx.window.draw(spr);

	//time
	sf::Text txt(timeStr, gfx.fontDigital, 120);
	sf::Vector2f off{ x + 80 * sx, y + 80 * sy };
	txt.setPosition(off.x, off.y);
	txt.setScale(sx, sy);
	gfx.window.draw(txt);

	//morning?
	txt.setString((isPM) ? "PM" : "AM");
	txt.setCharacterSize(50);
	sf::Vector2f off2{ off.x + 300 * sx, off.y + 120 * sy };
	txt.setPosition(off2);
	gfx.window.draw(txt);
}


//draw an old style clock
void Game::DrawAnalog(float x, float y, float sx, float sy, int hours24, int minutes, bool isPM)
{
	//background
	sf::Sprite spr(gfx.texAnalogFace);
	spr.setOrigin(0, 0);
	spr.setPosition(x, y);
	spr.setScale(sx, sx);
	gfx.window.draw(spr);

	//find centre of face
	sf::Vector2f centre{ spr.getPosition().x + (spr.getTextureRect().width / 2)*sx,
		spr.getPosition().y + (spr.getTextureRect().height / 2)*sy };

	float degH, degM;
	TimeToAngle(hours24, minutes, degH, degM);

	//hour hand
	gfx.sprHours.setPosition(centre);
	gfx.sprHours.setScale(sx*hoursScale, sy*hoursScale);
	gfx.sprHours.setRotation(degH);
	gfx.window.draw(gfx.sprHours);
	//minute hand
	gfx.sprMinutes.setPosition(centre);
	gfx.sprMinutes.setScale(sx*minutesScale, sy*minutesScale);
	gfx.sprMinutes.setRotation(degM);
	gfx.window.draw(gfx.sprMinutes);
}



int main()
{
	Game game;
	game.Initiallise("What time is it?");

	// Start the game loop 
	while (game.gfx.window.isOpen())
	{
		// Process events
		sf::Event event;
		while (game.gfx.window.pollEvent(event))
		{
			// Close window: exit
			if (event.type == sf::Event::Closed) 
				game.gfx.window.close();
			if (event.type == sf::Event::TextEntered)
			{
				if (event.text.unicode == GC::ESCAPE_KEY)
				{
					game.gfx.window.close();
				}
			}
		} 

		// Clear screen
		game.gfx.window.clear();

		game.Update();
		game.Render();

		// Update the window
		game.gfx.window.display();
	}

	return EXIT_SUCCESS;
}
